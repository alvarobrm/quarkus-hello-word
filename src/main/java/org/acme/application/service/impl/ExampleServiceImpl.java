package org.acme.application.service.impl;

import io.quarkus.panache.common.Page;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.acme.application.service.ExampleService;
import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.in.FiltersIDTO;
import org.acme.application.service.dto.in.PaginationIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.application.service.transformer.ExampleServiceTransformer;
import org.acme.domain.ExampleEntity;
import org.acme.domain.ExampleRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@ApplicationScoped
@Slf4j
public class ExampleServiceImpl implements ExampleService {

    @Inject
    ExampleRepository repository;

    @Inject
    ExampleServiceTransformer transformer;

    @Override
    public Single<ExampleODTO> save(ExampleIDTO exampleIDTO) {
        log.info("trying save {}", exampleIDTO.getName());
        Single<ExampleEntity> exampleEntity = repository.save(transformer.toEntity(exampleIDTO));
        log.info("saved {}", exampleEntity);
        return exampleEntity.map(transformer::toODTO);
    }

    @Override
    public Observable<ExampleODTO> findByFilters(FiltersIDTO filtersIDTO, PaginationIDTO pagination) {
        Map<String, Object> params = new HashMap<>();
        if (Objects.nonNull(filtersIDTO.getName())) {
            params.put("name", filtersIDTO.getName());
        }

        Page p = Page.of(pagination.getOffset() , pagination.getLimit());

        return repository.findByFilters(params, p)
                .map(transformer::toODTO);

    }

}
