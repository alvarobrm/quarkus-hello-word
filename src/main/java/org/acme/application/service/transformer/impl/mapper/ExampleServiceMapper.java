package org.acme.application.service.transformer.impl.mapper;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.domain.ExampleEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface ExampleServiceMapper {

    ExampleODTO toODTO(ExampleEntity exampleEntity);

    ExampleEntity toEntity(ExampleIDTO exampleIDTO);
}
