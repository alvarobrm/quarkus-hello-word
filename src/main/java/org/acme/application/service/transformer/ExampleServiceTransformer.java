package org.acme.application.service.transformer;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.domain.ExampleEntity;

public interface ExampleServiceTransformer {

    ExampleODTO toODTO(ExampleEntity exampleEntity);

    ExampleEntity toEntity(ExampleIDTO exampleIDTO);
}
