package org.acme.application.service.transformer.impl;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.application.service.transformer.ExampleServiceTransformer;
import org.acme.application.service.transformer.impl.mapper.ExampleServiceMapper;
import org.acme.domain.ExampleEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ExampleServiceTransformerImpl implements ExampleServiceTransformer {

    @Inject
    ExampleServiceMapper mapper;

    @Override
    public ExampleODTO toODTO(ExampleEntity exampleEntity) {
        return mapper.toODTO(exampleEntity);
    }

    @Override
    public ExampleEntity toEntity(ExampleIDTO exampleIDTO) {
        return mapper.toEntity(exampleIDTO);
    }
}
