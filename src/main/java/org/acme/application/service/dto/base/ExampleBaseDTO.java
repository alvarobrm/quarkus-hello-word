package org.acme.application.service.dto.base;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExampleBaseDTO {

    private String name;
}
