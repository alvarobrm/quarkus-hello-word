package org.acme.application.service.dto.in;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;

@Data
@NoArgsConstructor
public class PaginationIDTO {

    @QueryParam("offset")
    private Integer offset;

    @QueryParam("limit")
    private Integer limit;

}
