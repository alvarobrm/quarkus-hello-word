package org.acme.application.service.dto.in;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.acme.application.service.dto.base.ExampleBaseDTO;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class ExampleIDTO extends ExampleBaseDTO {


}
