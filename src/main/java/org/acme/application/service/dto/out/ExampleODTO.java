package org.acme.application.service.dto.out;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.acme.application.service.dto.base.ExampleBaseDTO;

@Data
@NoArgsConstructor
public class ExampleODTO extends ExampleBaseDTO {

    private Long id;
}
