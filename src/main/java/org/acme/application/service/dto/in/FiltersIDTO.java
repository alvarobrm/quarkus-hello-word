package org.acme.application.service.dto.in;

import io.reactivex.annotations.NonNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;

@Data
@NoArgsConstructor
public class FiltersIDTO {

    @QueryParam("name")
    @NonNull
    private String name;


}
