package org.acme.application.service;

import io.reactivex.Observable;
import io.reactivex.Single;
import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.in.FiltersIDTO;
import org.acme.application.service.dto.in.PaginationIDTO;
import org.acme.application.service.dto.out.ExampleODTO;

public interface ExampleService {

    Single<ExampleODTO> save(ExampleIDTO example);

    Observable<ExampleODTO> findByFilters(FiltersIDTO filters, PaginationIDTO pagination);

}
