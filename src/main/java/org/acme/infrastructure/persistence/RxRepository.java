package org.acme.infrastructure.persistence;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.acme.infrastructure.persistence.entity.ExampleJpaEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.UserTransaction;
import java.util.Map;

@Slf4j
@ApplicationScoped
public class RxRepository implements PanacheRepository<ExampleJpaEntity> {

    @Inject
    UserTransaction transaction;

    Scheduler scheduler = Schedulers.io();

    public RxRepository (){
        this.scheduler.start();
    }

    private String createQuery(Map<String, Object> params) {
        StringBuilder sb = new StringBuilder();
        params.keySet().forEach(key ->
                        sb.append(key).append("= :").append(key)
                               );
        return sb.toString();
    }

    public Observable<ExampleJpaEntity> findByFilters(Map<String, Object> params, Page page) {
        PanacheQuery<ExampleJpaEntity> query = find(createQuery(params), params).range(page.index, page.size);
        return Observable.fromIterable(query.list());
    }

    public Single<ExampleJpaEntity> save(ExampleJpaEntity exampleEntity) {

        Flowable<ExampleJpaEntity> flowable = Flowable.fromCallable(() -> {
            transaction.begin();
            persist(exampleEntity);
            transaction.commit();
            return exampleEntity;
        }).subscribeOn(scheduler);

        return flowable.firstOrError();
    }
}
