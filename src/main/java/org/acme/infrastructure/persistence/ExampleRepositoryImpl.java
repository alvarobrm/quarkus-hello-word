package org.acme.infrastructure.persistence;

import io.quarkus.panache.common.Page;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.acme.domain.ExampleEntity;
import org.acme.domain.ExampleRepository;
import org.acme.infrastructure.persistence.entity.ExampleJpaEntity;
import org.acme.infrastructure.persistence.transformer.ExampleTransformer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Map;
import java.util.Optional;

@ApplicationScoped
public class ExampleRepositoryImpl implements ExampleRepository {


    @Inject
    RxRepository repository;

    @Inject
    ExampleTransformer transformer;

    @Override
    public Observable<ExampleEntity> findByFilters(Map<String, Object> params, Page page) {
        return repository.findByFilters(params, page)
                .map(transformer::toEntity);
    }

    @Override
    public Single<ExampleEntity> save(ExampleEntity exampleEntity) {
        ExampleJpaEntity exampleJpaEntity = Optional.ofNullable(exampleEntity)
                .map(transformer::toJpaEntity)
                .orElseThrow(() -> new RuntimeException("Error"));
        return repository.save(exampleJpaEntity).map(transformer::toEntity);
    }
}
