package org.acme.infrastructure.persistence.transformer.impl.mapper;

import org.acme.domain.ExampleEntity;
import org.acme.infrastructure.persistence.entity.ExampleJpaEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface ExampleMapper {

    ExampleJpaEntity toJpaEntity(ExampleEntity exampleEntity);

    ExampleEntity toEntity(ExampleJpaEntity exampleJpaEntity);
}
