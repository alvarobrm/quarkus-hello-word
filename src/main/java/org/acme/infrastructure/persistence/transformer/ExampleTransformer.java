package org.acme.infrastructure.persistence.transformer;

import org.acme.domain.ExampleEntity;
import org.acme.infrastructure.persistence.entity.ExampleJpaEntity;

public interface ExampleTransformer {

    ExampleJpaEntity toJpaEntity(ExampleEntity exampleEntity);

    ExampleEntity toEntity(ExampleJpaEntity exampleJpaEntity);


}
