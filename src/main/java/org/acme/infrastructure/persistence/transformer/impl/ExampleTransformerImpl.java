package org.acme.infrastructure.persistence.transformer.impl;

import org.acme.domain.ExampleEntity;
import org.acme.infrastructure.persistence.entity.ExampleJpaEntity;
import org.acme.infrastructure.persistence.transformer.ExampleTransformer;
import org.acme.infrastructure.persistence.transformer.impl.mapper.ExampleMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ExampleTransformerImpl implements ExampleTransformer {

    @Inject
    private ExampleMapper mapper;

    @Override
    public ExampleJpaEntity toJpaEntity(ExampleEntity exampleEntity) {
        return mapper.toJpaEntity(exampleEntity);
    }

    @Override
    public ExampleEntity toEntity(ExampleJpaEntity exampleJpaEntity) {
        return mapper.toEntity(exampleJpaEntity);
    }
}
