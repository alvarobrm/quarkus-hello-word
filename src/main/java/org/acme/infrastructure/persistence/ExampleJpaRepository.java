package org.acme.infrastructure.persistence;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;
import io.vertx.reactivex.ext.sql.SQLClient;
import lombok.extern.slf4j.Slf4j;
import org.acme.infrastructure.persistence.entity.ExampleJpaEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@ApplicationScoped
@Slf4j
public class ExampleJpaRepository implements PanacheRepository<ExampleJpaEntity> {

    private String createQuery(Map<String, Object> params) {
        StringBuilder sb = new StringBuilder();
        params.keySet().forEach(key ->
                        sb.append(key).append("= :").append(key)
                               );
        return sb.toString();
    }

    public List<ExampleJpaEntity> findByFilters(Map<String, Object> params, Page page) {
        PanacheQuery<ExampleJpaEntity> query = find(createQuery(params), params).range(page.index, page.size);
        return query.list();
    }

    public ExampleJpaEntity save(ExampleJpaEntity exampleEntity) {

        persist(exampleEntity);
        log.info("trying save {}", exampleEntity);
        return exampleEntity;
    }
}
