package org.acme.infrastructure.messaging;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExampleMDTO {

    private String name;
}
