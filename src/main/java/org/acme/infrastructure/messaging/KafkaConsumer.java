package org.acme.infrastructure.messaging;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;
import org.acme.application.service.ExampleService;
import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.infrastructure.messaging.transformer.ExampleMessagingTransformer;
import org.acme.infrastructure.persistence.RxRepository;
import org.acme.infrastructure.persistence.entity.ExampleJpaEntity;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.streams.operators.ReactiveStreams;

import javax.inject.Inject;

@Slf4j
public class KafkaConsumer {

    public static final String EXAMPLE_TOPIC = "example-topic";

    @Inject
    ExampleService service;

    @Inject
    ExampleMessagingTransformer transformer;

    @Inject
    RxRepository rxRepository;


    @Incoming(EXAMPLE_TOPIC)
    public void consume(ExampleMDTO data) {
        log.info(data.toString());

        Single<ExampleODTO> single = service.save(transformer.toIDTO(data));
        single.subscribe(e -> log.info("id = {}", e.getId()));

    }

}
