package org.acme.infrastructure.messaging.config;

import lombok.Data;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
@Data
public class KafkaConsumerConfig {


    private String topic;
}
