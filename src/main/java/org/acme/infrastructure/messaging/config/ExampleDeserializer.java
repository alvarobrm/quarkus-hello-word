package org.acme.infrastructure.messaging.config;

import io.quarkus.kafka.client.serialization.JsonbDeserializer;
import org.acme.infrastructure.messaging.ExampleMDTO;

public class ExampleDeserializer extends JsonbDeserializer<ExampleMDTO> {

    public ExampleDeserializer() {
        super(ExampleMDTO.class);
    }
}
