package org.acme.infrastructure.messaging.transformer;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.infrastructure.messaging.ExampleMDTO;

public interface ExampleMessagingTransformer {

    ExampleIDTO toIDTO(ExampleMDTO exampleMDTO);
}
