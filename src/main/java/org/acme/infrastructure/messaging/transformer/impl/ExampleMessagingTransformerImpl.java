package org.acme.infrastructure.messaging.transformer.impl;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.infrastructure.messaging.ExampleMDTO;
import org.acme.infrastructure.messaging.transformer.ExampleMessagingTransformer;
import org.acme.infrastructure.messaging.transformer.impl.mapper.ExampleMessagingMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ExampleMessagingTransformerImpl implements ExampleMessagingTransformer {

    @Inject
    ExampleMessagingMapper mapper;

    @Override
    public ExampleIDTO toIDTO(ExampleMDTO exampleMDTO) {
        return mapper.toIDTO(exampleMDTO);
    }
}
