package org.acme.infrastructure.messaging.transformer.impl.mapper;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.infrastructure.messaging.ExampleMDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface ExampleMessagingMapper {

    ExampleIDTO toIDTO(ExampleMDTO exampleMDTO);
}
