package org.acme.infrastructure.resource;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Emitter;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.acme.application.service.ExampleService;
import org.acme.application.service.dto.in.FiltersIDTO;
import org.acme.application.service.dto.in.PaginationIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.infrastructure.resource.dto.in.ExampleRQDTO;
import org.acme.infrastructure.resource.dto.in.FiltersRQDTO;
import org.acme.infrastructure.resource.dto.in.PaginationRQDTO;
import org.acme.infrastructure.resource.dto.out.ExampleRSDTO;
import org.acme.infrastructure.resource.transformer.ExampleResourceTransformer;
import org.jboss.resteasy.annotations.SseElementType;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.concurrent.TimeUnit;

@Path("/example")
public class ExampleResource {

    Logger log = LoggerFactory.getLogger(ExampleResource.class);

    @Inject
    ExampleService service;

    @Inject
    ExampleResourceTransformer transformer;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Single<Response> saveExample(ExampleRQDTO exampleRQDTO) {
        log.info(exampleRQDTO.toString());
        Single<ExampleODTO> saved = service.save(transformer.toIDTO(exampleRQDTO));
        return saved.map(e -> Response.created(URI.create("/" + e.getId())).build());
    }

    @GET
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType(MediaType.APPLICATION_JSON)
    public Publisher<ExampleRSDTO> getExample(@BeanParam FiltersRQDTO filters, @BeanParam PaginationRQDTO pagination) {
        log.info(filters.getName());
        FiltersIDTO filtersIDTO = transformer.toIDTO(filters);
        PaginationIDTO paginationIDTO = transformer.toIDTO(pagination);
        Observable<ExampleODTO> byFilters = service.findByFilters(filtersIDTO, paginationIDTO);

        return byFilters.map(transformer::toRSDTO).toFlowable(BackpressureStrategy.BUFFER);
    }

}

