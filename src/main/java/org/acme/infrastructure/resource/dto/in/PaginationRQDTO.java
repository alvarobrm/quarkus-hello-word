package org.acme.infrastructure.resource.dto.in;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;

@Data
@NoArgsConstructor
public class PaginationRQDTO {

    @QueryParam("offset")
    private Integer offset;

    @QueryParam("limit")
    private Integer limit;

}
