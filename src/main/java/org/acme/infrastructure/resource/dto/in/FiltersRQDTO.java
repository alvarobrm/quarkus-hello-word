package org.acme.infrastructure.resource.dto.in;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;

@Data
@NoArgsConstructor
public class FiltersRQDTO {

    @QueryParam("name")
    private String name;


}
