package org.acme.infrastructure.resource.dto.base;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExampleBaseRQDTO {

    private String name;
}
