package org.acme.infrastructure.resource.dto.in;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.acme.infrastructure.resource.dto.base.ExampleBaseRQDTO;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class ExampleRQDTO extends ExampleBaseRQDTO {


}
