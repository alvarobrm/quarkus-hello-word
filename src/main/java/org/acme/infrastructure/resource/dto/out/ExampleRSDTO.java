package org.acme.infrastructure.resource.dto.out;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.acme.infrastructure.resource.dto.base.ExampleBaseRQDTO;

@Data
@NoArgsConstructor
public class ExampleRSDTO extends ExampleBaseRQDTO {

    private Long id;
}
