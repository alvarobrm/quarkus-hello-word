package org.acme.infrastructure.resource.transformer.impl;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.in.FiltersIDTO;
import org.acme.application.service.dto.in.PaginationIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.infrastructure.resource.dto.in.ExampleRQDTO;
import org.acme.infrastructure.resource.dto.in.FiltersRQDTO;
import org.acme.infrastructure.resource.dto.in.PaginationRQDTO;
import org.acme.infrastructure.resource.dto.out.ExampleRSDTO;
import org.acme.infrastructure.resource.transformer.ExampleResourceTransformer;
import org.acme.infrastructure.resource.transformer.impl.mapping.ExampleResourceMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ExampleResourceTransformerImpl  implements ExampleResourceTransformer {

    @Inject
    ExampleResourceMapper mapper;

    @Override
    public ExampleRSDTO toRSDTO(ExampleODTO example) {
        return mapper.toRSDTO(example);
    }

    @Override
    public ExampleIDTO toIDTO(ExampleRQDTO example) {
        return mapper.toIDTO(example);
    }

    @Override
    public FiltersIDTO toIDTO(FiltersRQDTO filtersIDTO) {
        return mapper.toIDTO(filtersIDTO);
    }

    @Override
    public PaginationIDTO toIDTO(PaginationRQDTO paginationRQDTO) {
        if (paginationRQDTO.getLimit() == null){
            paginationRQDTO.setLimit(200);
        }
        if (paginationRQDTO.getOffset() == null) {
            paginationRQDTO.setOffset(0);
        }
        return mapper.toIDTO(paginationRQDTO);
    }
}
