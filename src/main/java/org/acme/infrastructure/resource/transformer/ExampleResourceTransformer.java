package org.acme.infrastructure.resource.transformer;

import org.acme.application.service.dto.in.ExampleIDTO;
import org.acme.application.service.dto.in.FiltersIDTO;
import org.acme.application.service.dto.in.PaginationIDTO;
import org.acme.application.service.dto.out.ExampleODTO;
import org.acme.infrastructure.resource.dto.in.ExampleRQDTO;
import org.acme.infrastructure.resource.dto.in.FiltersRQDTO;
import org.acme.infrastructure.resource.dto.in.PaginationRQDTO;
import org.acme.infrastructure.resource.dto.out.ExampleRSDTO;

public interface ExampleResourceTransformer {

    ExampleRSDTO toRSDTO (ExampleODTO example);

    ExampleIDTO toIDTO(ExampleRQDTO example);

    FiltersIDTO toIDTO(FiltersRQDTO filtersIDTO);

    PaginationIDTO toIDTO (PaginationRQDTO paginationRQDTO);
}
