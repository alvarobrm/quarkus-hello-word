package org.acme.domain;

import io.quarkus.panache.common.Page;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.util.Map;

public interface ExampleRepository {

    Observable<ExampleEntity> findByFilters(Map<String, Object> params, Page page);

    Single<ExampleEntity> save(ExampleEntity exampleEntity);
}
